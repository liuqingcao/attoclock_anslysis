function abel_inversion
clear all;
close all;
clc;
load('clipImage.mat');

intAngleHalf = 25;
rotate = 90;
peakList = [15,39,52,63];
sym = 0;
%wavelength
wlgth=780e-9;
%
c=2.9979e8;
omega=2*pi*c/wlgth;
e=1.60218e-19;
m=9.10938291e-31;
e_0=8.854187e-12;
I_p=12.12984*e;     %Xenon
h=6.62607e-34;
phot=h/2/pi*omega;
photEnergy = phot/e;


%
xAxis = [-screenRadius:screenRadius];
yAxis = [-screenRadius:screenRadius];
xCenter = screenRadius+1;
yCenter = screenRadius+1;

if(0)
    currentImage = zeros(285,285);
    currentImage(143-20:143+20,:) = 1000;
    currentImage = fast_inversion(currentImage,sym);
else
    currentImage = fast_inversion(clipImage,sym);
end

%convert to cart
ang_image = streaking_cycle_convert(log10(currentImage),xCenter,yCenter);
%integral along radius
streakLeft = streaking_cycle_angle(ang_image, rotate, intAngleHalf,0);
streakRight = streaking_cycle_angle(ang_image, rotate, intAngleHalf,1);
size(streakLeft)
%fit
fitResult = fit([1:length(peakList)]',peakList.^2','poly1');
a = confint(fitResult);
p1Error = a(:,1);
epp = photEnergy/fitResult.p1;
eppError = photEnergy./p1Error;
mpp = sqrt(epp/13.6055);


%plot summed image
figure('position',get(0,'screenSize'),'color',[1 1 1]);
subplot(5,5,[1:3,6:8,11:13,16:18,21:23]);
imagesc(xAxis,yAxis,log10(currentImage));
axis equal;
hold on;
scatter(0,0,10);
for i=1:length(peakList)
    rectangle('Position',[-peakList(i),- peakList(i),2*peakList(i),2*peakList(i)], ...
        'Curvature',[1,1],'edgecolor','w','linestyle','-');
end
rectangle('Position',[- screenRadius,- screenRadius,2*screenRadius,2*screenRadius], ...
    'Curvature',[1,1],'edgecolor','k','linewidth',2);
plot3([0,0],get(gca,'ylim'),[100,100],'k');
plot3(get(gca,'xlim'),[0,0],[100,100],'k');
plot3([-screenRadius*cosd(intAngleHalf),screenRadius*cosd(intAngleHalf)], ...
      [-screenRadius*sind(intAngleHalf),screenRadius*sind(intAngleHalf)], [100,100],'k');
plot3([-screenRadius*cosd(180-intAngleHalf),screenRadius*cosd(180-intAngleHalf)], ...
      [-screenRadius*sind(180-intAngleHalf),screenRadius*sind(180-intAngleHalf)], [100,100],'k');
plot3([0,0],get(gca,'ylim'),[100,100],'k');
set(gca,'ydir','norm');
xlim([-screenRadius,screenRadius]);
ylim([-screenRadius,screenRadius]);
xlabel('X (pixel)');
ylabel('Y (pixel)');
%

subplot(5,5,[4:5,9:10]);
plot(streakLeft,'r');
hold on;
plot(streakRight,'b');
for i=1:length(peakList)
    plot([peakList(i),peakList(i)],get(gca,'ylim'),'--k');
end
xlim([0,screenRadius]);
ylim([0,inf]);
xlabel('Radius (pixel)');
ylabel('');


subplot(5,5,[14:15]);
scatter([1:length(peakList)],peakList.^2,'ob');
hold on;
plot([1:length(peakList)],fitResult([1:length(peakList)]),'r');
box on;
text(0.01,0.95,['Wavelength: ',num2str(wlgth*1e9),' nm (',num2str(photEnergy,'%5.2f'),' eV)'],'units','normalized','fontsize',12);
text(0.01,0.8,['Momentum scale: ',num2str(mpp,'%6.2e'),' a.u./pixel'],'units','normalized','fontsize',12);
text(0.01,0.65,['Energy scale: ',num2str(epp,'%6.2e'),' eV/pixel^{2}'],'units','normalized','fontsize',12);


subplot(5,5,[19:20,24:25]);
plot(([0:screenRadius]+1)*mpp,0.5*(streakLeft+streakRight),'r');
hold on;
for i=1:length(peakList)
    plot([peakList(i),peakList(i)]*mpp,get(gca,'ylim'),'--k');
end
disp(num2str(peakList*mpp));
xlim(([0,screenRadius]+1)*mpp);
xlabel('Momentum (a.u.)');
ylabel('');

saveas(gcf,fullfile(dataFolder,'abel_inversion.fig'));

diff(peakList.^2 * epp)

    
function abel_arr = fast_inversion(work_im,sym)
    
    sym_lr=0;
    sym_ud=0;
    % Symmetrize Left and Right
    if(sym > 0)
        sym_lr=1;
    end
    % Symmetrize Up and Down
    if(sym > 1)
        sym_ud=1;
    end


    % find dimensions: 
    dim=size(work_im);
    ymax=dim(1); % number of rows (y-coordinate)
    if sym_ud == 1
        work_im = work_im(1:fix(ymax/2),:);
    end

    % update dimensions: 
    dim=size(work_im);
    ymax=dim(1); % number of rows (y-coordinate)
    xmax=dim(2); % number of columns (x-coordinate)
    
    % update center: 
    xcl=fix(xmax/2);
    xcr=fix(xmax/2)+1;
    if sym_ud~=1,yc=fix(ymax/2)+1;end
    if sym_ud==1,yc=fix(ymax)+1;end
    
    % Initialize abel routine 
    [val1,val2]=init_abel(xcl,yc);

    abel_arr=work_im*0;
    rest_arr=work_im;
    vect=(1:ymax)*0;

    % process right half of the image
    for col_index = xmax:-1:xcr
        idist = col_index-xcr;
        rest_row = rest_arr(:,col_index);
        normfac = 1/val1(idist+1,idist+1);
        for i = idist:-1:0
            ic = xcr + i;
            shadow=rest_row*normfac*val1(i+1,idist+1);
            rest_arr(:,ic)=rest_arr(:,ic)-shadow;
           rest_arr=max(rest_arr,0);           % Fastest
     %        rest_arr(rest_arr<0)=0;             % Second Fastest
      %        rest_arr = rest_arr.*(rest_arr>0);   % Original
        end
        for row_index = 1:ymax
            jdist=abs(row_index-yc);
            vect(row_index)=val2(idist+1,jdist+1);
        end
        abel_arr(:,col_index)=normfac*rest_row.*vect';
    end

    % process left half of the image

    if sym_lr ~=1
        for col_index = 1:xcl
            idist = xcl-col_index;
            rest_row = rest_arr(:,col_index);
            normfac = 1/val1(idist+1,idist+1);
            for i = idist:-1:0
                ic = xcl-i;
                shadow=rest_row*normfac*val1(i+1,idist+1);
                rest_arr(:,ic)=rest_arr(:,ic)-shadow;
                temp = rest_arr>0;
                rest_arr=max(rest_arr,0);         % Fastest
     %            rest_arr(rest_arr<0)=0;           % Second Fastest
     %            rest_arr = rest_arr.*(rest_arr>0); % Original
            end
            for row_index = 1:ymax
                jdist=abs(row_index - yc);
                vect(row_index)=val2(idist+1,jdist+1);
            end
            abel_arr(:,col_index+1)=normfac*rest_row.*vect';
        end
    end

if sym_ud ==1,abel_arr=[abel_arr;flipdim(abel_arr,1)];end
if sym_lr ==1,abel_arr=abel_arr+flipdim(abel_arr,2);end


function [val1,val2]=init_abel(xcg,ycg)
xc = double(xcg);
yc = double(ycg);
val1=zeros(xc+1,xc+1);
val2=zeros(xc+1,yc+1);

for ii = 1: xc+1
    for  jj = ii: xc+1
        val1(ii,jj)=asin(ii/jj)-asin((ii-1)/jj);
    end
end
for idist = 1:xc+1
    for  jdist = 1:yc+1
        val2(idist,jdist)=sqrt(idist^2+(jdist-1)^2)/idist;
    end
end


function create_symImage
    global invSettings;
    x1 = invSettings.vCenter-invSettings.vRange;
    x2 = invSettings.vCenter+invSettings.vRange-1;
    y1 = invSettings.hCenter-invSettings.hRange;
    y2 = invSettings.hCenter+invSettings.hRange-1;
    invSettings.SymImage = invSettings.delayImage(:,x1:x2,y1:y2);
    if(invSettings.Symmetry > 0)
        invSettings.SymImage = 0.5*(invSettings.SymImage+flipdim(invSettings.SymImage,2));
    end
    if(invSettings.Symmetry > 1)
        invSettings.SymImage = 0.5*(invSettings.SymImage+flipdim(invSettings.SymImage,1));    
    end  





function ang_image = streaking_cycle_convert(dataImage,xCenter,yCenter)
    sx=size(dataImage,2);
    sy=size(dataImage,1);
    X=(1:sx) - xCenter;
    Y=(1:sy) - yCenter;
    thetaDeg = [0:1:359];
    theta=deg2rad(thetaDeg);
    R=0:xCenter-1;
    for i=1:length(theta)
        [xprime(:,i),yprime(:,i)]=pol2cart(theta(i),R);
    end
	ang_image = interp2(X,Y,dataImage,xprime,yprime,'*linear');
    ang_image(isnan(ang_image)) = 0;
    ang_image(isinf(ang_image)) = 0;
    
function streaking = streaking_cycle_angle(AngleImage, rotate, StreakAngle,updown)
    if(updown == 1) %part-1
        ang1 = 90 - rotate - StreakAngle;
        ang2 = 90 - rotate + StreakAngle;
    else %the opposite part
        ang1 = 270 - rotate - StreakAngle;
        ang2 = 270 - rotate + StreakAngle;      
    end
    if(ang1 <= 0)
        ang1 = ang1 + 360;
    end
    if(ang2 <= 0)
        ang2 = ang2 + 360;
    end
    if(ang1 < ang2) % normal
        streaking = sum(AngleImage(:,ang1:ang2),2);
    else % cross the 360 degree
        streaking = sum(AngleImage(:,ang1:360),2);
        streaking = streaking + sum(AngleImage(:,1:ang2),2);
    end
    
function rad = deg2rad(deg)
    rad = (pi/180).*deg;








