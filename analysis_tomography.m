function analysis_tomography
global scanSettings;
global tomo;
assignin('base','tomo',tomo);
assignin('base','scanSettings',scanSettings);
tomo = [];
tic;

%define data folder
% tomo.dataFolder = 'D:\Project_20171203\Recording_{1BD4E52A-55CA-47B4-B58F-93BCBF5CAFF9}';
tomo.dataFolder = 'I:\w2w_linear2\Project_NewProject170724\Recording_{9E50ABCF-41F5-4B68-B85D-1A0C987F3280}';

%load in scan file
load(fullfile(tomo.dataFolder,'scan.mat'));
%find the raw data file
temp = strfind(scanSettings.DataPath,'\');
tomo.dataPath =  fullfile(tomo.dataFolder,scanSettings.DataPath(temp(end)+1:end));
%date information
try
disp(['This scan starts around ',datestr(scanSettings.timeStamp(:,1)')])
end
%check the preview figure
if(0)
    figure;
    subplot(221);
    imagesc(scanSettings.delays,scanSettings.streakRadius,log10(scanSettings.PStreaking'));
    set(gca,'ydir','norm');
    subplot(222);
    imagesc(log10(scanSettings.summed_image));
    set(gca,'ydir','norm');
    subplot(223);
    plot(scanSettings.delayHistogram(1,:,2));
    set(gca,'yscale','log');
    subplot(224);
    plot(mean(scanSettings.delayHistogram(:,:,2),1));
    set(gca,'yscale','log');
    return
end

%specific the image center
tomo.hCenter = 239;
tomo.vCenter = 418;
%specific the streaking parameters
tomo.halfConeAngle = 25;
tomo.rotateAngle = 90;
tomo.streakRadius = 200;
tomo.hResolution = scanSettings.hResolution;
tomo.vResolution = scanSettings.vResolution;

%period
tomo.period = 90;
%data range for tomography
tomo.startTime = 2.0;
tomo.timeDuration = tomo.period*2;

%tomography angles
tomo.angles = 180;

%switches
tomo.shift = 0; %shift the data by average
tomo.norm = 0; %normalized the image to the total counts
tomo.overlay = 0; %sum up the images if you repeat the scan
tomo.overlayShift = 1; %shift the steps to overlap the steps
tomo.normBeforeShift = 0; %normalized the data before shift
%specific the image source
imageSource = 1;
switch imageSource
    case 0%use the old images
        tomo.periodIndex = [60,100];
    case 1%the left part of the histogram
        tomo.EventsLower = 5;
        tomo.EventsUpper = 50;
        tomo.periodIndex = [40,75]; %integral radius range to determin the periodicity
    case 2%the right part of the histogram
        tomo.EventsLower = 60;
        tomo.EventsUpper = 1020;
        tomo.periodIndex = [75,100];
end

%integral pixel width along propagation direction for tomography 
tomo.sliceIndex = tomo.vCenter; %center pixel
tomo.width = 10; %width

% convert x-axis
tomo.dist2time = 1; %nm/fs for w-2w, ang/ang for attoclock
tomo.timeStepSize = scanSettings.delayStep;
tomo.timesOld = scanSettings.delays/tomo.dist2time;


%% get the images
if(imageSource == 0)
    tomo.EventsLower = 0;
    tomo.EventsUpper = scanSettings.thresholdReductionSize-1;
    tomo.imagesOld = scanSettings.delayImage;
else
    %if the data is not exist, then use the c++ code to read the raw camera
    %file
    fileName = fullfile(tomo.dataFolder,['images_',num2str(tomo.EventsLower),'_',num2str(tomo.EventsUpper),'.mat']);
    if(~exist(fileName,'file'))
        rerange_raw_data(tomo.EventsLower,tomo.EventsUpper);
    end
    %read in the new images
    data = load(fileName);
    tomo.imagesOld = data.delayImage;
end

%% delete the noises on the boarder
boarderSize = 10;
tomo.imagesOld(:,1:boarderSize,:) = 0;
tomo.imagesOld(:,:,1:boarderSize) = 0;
tomo.imagesOld(:,(tomo.hResolution-boarderSize):end,:) = 0;
tomo.imagesOld(:,:,(tomo.vResolution-boarderSize):end) = 0;


%% normalize images
if(tomo.normBeforeShift && tomo.norm)
    disp(['Normalized the image to the total yield BEFORE superposition!']);
    for i=1:length(tomo.timesOld)
        if(tomo.norm)
            tomo.imagesNorm(i, :, :) = tomo.images(i, :, :)./sum(sum(tomo.images(i, :, :),3),2);
        end
    end
end



%% superposition the delay
if(tomo.overlay)
    delays = scanSettings.startDelay:scanSettings.delayStep:scanSettings.endDelay;
    tomo.times = delays/tomo.dist2time;
    disp(['Superposition the delays by ',num2str(tomo.overlayShift),' step shift']);
    if(length(tomo.timesOld) == 2*length(tomo.times)-1)  
        if(tomo.overlayShift == 0) %no shift
           for i=1:length(tomo.times)
               tomo.images(i,:,:) = tomo.imagesOld(i,:,:) + tomo.imagesOld(2*length(tomo.times)-i,:,:);
               disp(['superposition index: [',num2str(i),',',num2str(2*length(tomo.times)-i),']']); 
           end        
        elseif(tomo.overlayShift > 0) %shift positive
           for i=1+tomo.overlayShift:length(tomo.times)
               tomo.images(i,:,:) = tomo.imagesOld(i,:,:) + tomo.imagesOld(2*length(tomo.times)+tomo.overlayShift-i,:,:);
               disp(['superposition index: [',num2str(i),',',num2str(2*length(tomo.times)+tomo.overlayShift-i),']']); 
           end
            for i=1:tomo.overlayShift
                tomo.images(i,:,:) = tomo.imagesOld(i,:,:) * 2;
            end   
        elseif(tomo.overlayShift < 0) %shift negative
           for i=1:length(tomo.times)+tomo.overlayShift
               tomo.images(i,:,:) = tomo.imagesOld(i,:,:) + tomo.imagesOld(2*length(tomo.times)+tomo.overlayShift-i,:,:);
               disp(['superposition index: [',num2str(i),',',num2str(2*length(tomo.times)+tomo.overlayShift-i),']']); 
           end
           for i=length(tomo.times)+tomo.overlayShift+1:length(tomo.times)
               tomo.images(i,:,:) = tomo.imagesOld(i,:,:) * 2;
           end 
        end
    else
        disp('Superposition the delays failed, wrong delay steps');
        tomo.times = tomo.timesOld;
        tomo.images = tomo.imagesOld;
    end
else
    tomo.times = tomo.timesOld;
    tomo.images = tomo.imagesOld;
end


%if the scan range is from 0 to 90 degree, then repeat the data
% 
% tomo.times = [tomo.times,tomo.times(2:end-1) + 92];
% tomo.times = tomo.times*2;
% tomo.images = [tomo.images;tomo.images(2:end-1,:,:)];
% 



tomo.timeSteps = length(tomo.times);


%% find the interesting part
[~,tomo.index1] = min(abs(tomo.times - tomo.startTime));
[~,tomo.index2] = min(abs(tomo.times - (tomo.startTime+tomo.timeDuration)));
disp(['Data range: from ',num2str(tomo.times(tomo.index1)),' to ',num2str(tomo.times(tomo.index2)), '']);


% normalize images
if(~tomo.normBeforeShift)
    if(tomo.norm)
        disp(['Normalized the image to the total yield.']);
    end
    for i=1:tomo.timeSteps
        if(tomo.norm)
            tomo.imagesNorm(i, :, :) = tomo.images(i, :, :)./sum(sum(tomo.images(i, :, :),3),2);
        else
            tomo.imagesNorm(i, :, :) = tomo.images(i, :, :);
        end
    end
end

%% get streaking asy map
tomo.streaking = zeros(tomo.timeSteps,2*tomo.streakRadius+1);
tomo.asyMap = zeros(tomo.timeSteps,tomo.streakRadius+1);
for i=1:tomo.timeSteps
    [tomo.streaking(i,:), tomo.asyMap(i,:)] = streak_process(squeeze(tomo.imagesNorm(i, :, :)),tomo.hCenter,tomo.vCenter,tomo.streakRadius,tomo.rotateAngle,tomo.halfConeAngle);
end
%shift streaking and asy map
if(tomo.shift)
    meanStreak = mean(tomo.streaking(tomo.index1:tomo.index2,:),1);
    tomo.streaking = tomo.streaking - repmat(meanStreak,[tomo.timeSteps,1]);
    meanMap = mean(tomo.asyMap(tomo.index1:tomo.index2,:),1);
    tomo.asyMap = tomo.asyMap - repmat(meanMap,[tomo.timeSteps,1]);    
end


%% find periodicity
tomo.asyOne = mean(tomo.asyMap(:,tomo.periodIndex(1):tomo.periodIndex(2)),2);
tomo.asyOne = tomo.asyOne - mean(tomo.asyOne);
if(~tomo.overlay && scanSettings.goBack)
    tomo.asyTwo = mean(tomo.asyMap(:,tomo.periodIndex(1):tomo.periodIndex(2)),2);
end
%sin fitting
tomo.fitTime = tomo.times(tomo.index1:tomo.index2);
tomo.asyFit = tomo.asyOne(tomo.index1:tomo.index2);
[x,y] = prepareCurveData(tomo.fitTime,tomo.asyFit);
try
[tomo.fitParam,tomo.func] = sine_fit(x,y,[],[ -0.0067    0.0241         0    0.0100]);
end

%% tomography
%angle for tomography
tomo.angleAxis = (tomo.times(tomo.index1:tomo.index2) - tomo.times(tomo.index1))./tomo.period*tomo.angles;
%tomography for mean slices
tomo.sliceMean = squeeze(mean(tomo.imagesNorm(tomo.index1:tomo.index2,tomo.sliceIndex-tomo.width:tomo.sliceIndex+tomo.width,tomo.hCenter-tomo.streakRadius:tomo.hCenter+tomo.streakRadius),2));
if(tomo.shift)
    tomo.sliceMean = tomo.sliceMean - repmat(mean(tomo.sliceMean,1),[tomo.index2-tomo.index1+1,1]);
    tomo.sliceMean = tomo.sliceMean * 1e6;
    tomo.tomoMean = back(tomo.sliceMean', tomo.angleAxis)* 1e6;
    tomo.tomoMean(tomo.tomoMean<0) = 0;
else
    tomo.tomoMean = back(tomo.sliceMean', tomo.angleAxis);
end
ang_image = streaking_convert(tomo.tomoMean,tomo.streakRadius+1,tomo.streakRadius+1,tomo.streakRadius);
tomo.tomoMeanPolar = ang_image - repmat(mean(ang_image,2),[1,size(ang_image,2)]);




%% plot
figure('number','off','name',[num2str(tomo.EventsLower),'_',num2str(tomo.EventsUpper)],'color',[1 1 1]);
% figure('number','off','name',[num2str(tomo.EventsLower),'_',num2str(tomo.EventsUpper)],'position',get(0,'screenSize'),'color',[1 1 1]);
drawnow;
%plot streaking
subplot(331);
if(tomo.shift)
    imagesc(tomo.times,[-tomo.streakRadius:tomo.streakRadius],-sign((tomo.streaking')).*log10(abs(tomo.streaking')));
else
    imagesc(tomo.times,[-tomo.streakRadius:tomo.streakRadius],log10(tomo.streaking'));
end
hold on;
plot3([tomo.times(tomo.index1),tomo.times(tomo.index1)],get(gca,'ylim'),[100,100],'k');
plot3([tomo.times(tomo.index2),tomo.times(tomo.index2)],get(gca,'ylim'),[100,100],'k');
set(gca,'ydir','normal');
xlabel('time [fs]');
ylabel('radius');
drawnow;

%plot asy map
subplot(332);
imagesc(tomo.times,[0:tomo.streakRadius],tomo.asyMap');
set(gca,'ydir','normal');
% caxis([-0.7,0.7]);
hold on;
plot3([tomo.times(tomo.index1),tomo.times(tomo.index1)],get(gca,'ylim'),[100,100],'k');
plot3([tomo.times(tomo.index2),tomo.times(tomo.index2)],get(gca,'ylim'),[100,100],'k');
plot3(get(gca,'xlim'),[tomo.periodIndex(1),tomo.periodIndex(1)],[100,100],'--k');
plot3(get(gca,'xlim'),[tomo.periodIndex(2),tomo.periodIndex(2)],[100,100],'--k');
xlabel('time [fs]');
ylabel('radius');
drawnow;

%plot average image
subplot(333);
imagesc([-tomo.streakRadius:tomo.streakRadius],[-tomo.streakRadius:tomo.streakRadius], ...
        log10(squeeze(mean(tomo.imagesNorm(:,tomo.vCenter-tomo.streakRadius:tomo.vCenter+tomo.streakRadius,tomo.hCenter-tomo.streakRadius:tomo.hCenter+tomo.streakRadius),1))));
set(gca,'ydir','normal');
axis equal;
hold on;
tomo.sliceLow = tomo.sliceIndex - tomo.vCenter - tomo.width;
tomo.sliceUp = tomo.sliceIndex - tomo.vCenter + tomo.width;
plot3(get(gca,'xlim'),[tomo.sliceLow,tomo.sliceLow],[100,100],'--k');
plot3(get(gca,'xlim'),[tomo.sliceUp,tomo.sliceUp],[100,100],'--k');
xlabel('radius');
ylabel('radius');
drawnow;

%average slices
subplot(334);
if(tomo.shift)
    imagesc(tomo.times(tomo.index1:tomo.index2),[-tomo.streakRadius:tomo.streakRadius], ...
        (-sign(tomo.sliceMean) .* log10(abs(tomo.sliceMean)))');
else
    imagesc(tomo.times(tomo.index1:tomo.index2),[tomo.hCenter-tomo.streakRadius:tomo.hCenter+tomo.streakRadius],log10(tomo.sliceMean)');

end
set(gca,'ydir','normal');
% caxis([-1,1]);
xlabel('time [fs]');
ylabel('radius');
drawnow;

subplot(335);
imagesc([-tomo.streakRadius:tomo.streakRadius],[-tomo.streakRadius:tomo.streakRadius],tomo.tomoMean');
% imagesc([-tomo.streakRadius:tomo.streakRadius],[-tomo.streakRadius:tomo.streakRadius],log10(tomo.tomoMean)');

set(gca,'ydir','normal');
% caxis([-1,1]);
axis equal;
xlabel('radius');
ylabel('radius');
% xlim([-65,65]);
% ylim([-65,65]);
drawnow;

subplot(336);
imagesc([0:size(tomo.tomoMeanPolar,2)-1],[0:tomo.streakRadius],tomo.tomoMeanPolar);
set(gca,'ydir','normal');
% caxis([-1,1]);
xlabel('angle [deg]');
ylabel('radius');
xlim([0,360]);
% ylim([0,65])
drawnow;

%plot curve
subplot(337);
try
plot(tomo.fitTime,tomo.asyFit(1:length(tomo.fitTime)),'--',tomo.fitTime,tomo.func(tomo.fitParam,tomo.fitTime),'r');
% 
text(0.1,0.1,num2str(1/tomo.fitParam(4)),'units','normal');
xlim([tomo.fitTime(1),tomo.fitTime(end)]);
xlabel('time [fs]');
ylabel('average asy');
drawnow;
end

subplot(338);
plot(tomo.times,tomo.asyOne,'b');
try
if(scanSettings.goBack)
    hold on;
    plot(tomo.times,flipud(tomo.asyTwo),'r');
end
end
xlim([tomo.times(1),tomo.times(end)]);
xlabel('time [fs]');
ylabel('average asy');
drawnow;





%
toc;
%
fileName = fullfile(tomo.dataFolder,['tomo_',num2str(tomo.EventsLower),'_',num2str(tomo.EventsUpper)]);
saveas(gcf,[fileName,'.fig']);
% save([fileName,'.mat','tomo','scanSettings']);






















%% sub functions
function [streaking,asyMap] = streak_process(currentImage,hCenter,vCenter,streakRadius,rotateAngle,halfConeAngle)
    %create the angle map
	AngleImage = streaking_convert(currentImage,hCenter,vCenter,streakRadius);
    streaking1 = streaking_cycle_angle(AngleImage,1,rotateAngle,halfConeAngle);
    streaking2 = streaking_cycle_angle(AngleImage,0,rotateAngle,halfConeAngle);
    asyMap = (streaking1 - streaking2)./(streaking1 + streaking2 + 1e-5);
    streaking2 = flipud(streaking2);
    streaking2(end) = [];
    streaking = [streaking2;streaking1];

    

    
    
function ang_image = streaking_convert(currentImage,hCenter,vCenter,streakRadius)
    sx=size(currentImage,2);
    sy=size(currentImage,1);
    X=(1:sx)-hCenter;
    Y=(1:sy)-vCenter;
    theta=deg2rad([0:359]);
    R=0:streakRadius;
    for i=1:length(theta)
        [xprime(:,i),yprime(:,i)]=pol2cart(theta(i),R);
    end
	ang_image = interp2(X,Y,currentImage,xprime,yprime,'*linear');
    
    
    
    
function [ streaking ] = streaking_cycle_angle(AngleImage,updown,rotateAngle,halfConeAngle)
    if(updown == 1) %upper part
        ang1 = 90 - rotateAngle - halfConeAngle;
        ang2 = 90 - rotateAngle + halfConeAngle;
    else %the opposite part
        ang1 = 270 - rotateAngle - halfConeAngle;
        ang2 = 270 - rotateAngle + halfConeAngle;      
    end
    if(ang1 <= 0)
        ang1 = ang1 + 360;
    end
    if(ang2 <= 0)
        ang2 = ang2 + 360;
    end
    
    if(ang1 < ang2) % normal
        streaking = sum(AngleImage(:,ang1:ang2),2);
    else % cross the 360 degree
        streaking = sum(AngleImage(:,ang1:360),2);
        streaking = streaking + sum(AngleImage(:,1:ang2),2);
    end    


function rad = deg2rad(deg)
    rad = (pi/180).*deg;

