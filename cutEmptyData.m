function [ output_args ] = cutEmptyData()
%CUTEMPTYDATA Summary of this function goes here
%   Detailed explanation goes here
    global scanSettings;
    for i=1:numel(scanSettings.AsyMap(:,1))
        if sum(scanSettings.AsyMap(i,:)) == 0
            scanSettings.totalSteps = i-1;
            scanSettings.delays = scanSettings.delays(1:i-1);
            return
        end
    end

end

