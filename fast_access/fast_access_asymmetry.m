

dataFolder = 'D:\Project_2017_11_08\Recording_{307FF58D-E1B2-48CA-B36F-82FC6C798668}';
fileName = 'Datafile_{D24CED75-7787-4741-83D8-2AFD5B713366}.0';

% load(fullfile(dataFolder,'scan.mat'));


dataFolder = 'D:\Project_2017_11_08\Recording_{F1123678-979C-438B-828B-7A5824D5FA83}';
fileName = 'Datafile_{CFCB5C59-FF1D-4E47-8E49-223309A8CBCC}.0';


dataFolder = 'D:\Project_2017_11_08\Recording_{6974B3EE-3B07-48D5-9C44-55289E4ACD0F}';
fileName = 'Datafile_{835FFFED-D4A7-43A9-8785-FD6BDBEFE06E}.0';


dataFolder = 'D:\Project_2017_11_08\Recording_{C4096E86-3184-4973-9713-306A0E9E5FC6}';
fileName = 'Datafile_{0E91E565-8C15-4101-9CCC-EEBC57D09E5D}.0';



currentImage = zeros(800,600);
histogram = zeros(1025,2);
asymmetry = zeros(201,1);

startInput = 1;
endInput = 500000;

doAsymmetry = 1;
hCenter = 242;
vCenter = 419;
rotate = 0;
rangeMin = 50;
rangeMax = 100;
rangeWidth = 30;

command = ['.\tool\vmi_analysis_streak_full.exe', ...
            ' -s', num2str(startInput), ...
            ' -e', num2str(endInput), ...
            ' -d', num2str(1), ...
            ' -p', dataFolder, ...
            ' -g', num2str(0), ...
            ' -t', num2str(1024), ...
            ' -f', fullfile(dataFolder,fileName), ...
            ' -o', dataFolder, ...
            ' -h', num2str(800), ...
            ' -v', num2str(600), ...
            ' -a', num2str(0), ...
            ' -b',num2str(1020), ...
            ' -m', num2str(doAsymmetry), ...
            ' -x', num2str(vCenter), ...
            ' -y', num2str(hCenter), ...
            ' -l', num2str(rangeMin), ...
            ' -u', num2str(rangeMax), ...
            ' -r', num2str(rotate), ...
            ' -w', num2str(rangeWidth) ...
            ]
            
system(command);
[currentImage, histogram] = getImageHist(1);

if(doAsymmetry)
   asymmetry = getAsymmetry(1);
end




close all;
figure;
subplot(221);
imagesc(log10(currentImage));set(gca,'Ydir','normal');
hold on;
try
r = rangeMin;
rectangle('Position',[hCenter-r,vCenter-r,2*r,2*r],'Curvature',[1,1]);
end
try
r = rangeMax;
rectangle('Position',[hCenter-r,vCenter-r,2*r,2*r],'Curvature',[1,1]);
end
if(rotate)
    plot3([hCenter-rangeWidth,hCenter+rangeWidth],[vCenter-rangeMin,vCenter-rangeMin],[100,100],'-r');
    plot3([hCenter-rangeWidth,hCenter+rangeWidth],[vCenter-rangeMax,vCenter-rangeMax],[100,100],'-r');
    plot3([hCenter-rangeWidth,hCenter+rangeWidth],[vCenter+rangeMin,vCenter+rangeMin],[100,100],'-r');
    plot3([hCenter-rangeWidth,hCenter+rangeWidth],[vCenter+rangeMax,vCenter+rangeMax],[100,100],'-r');
    plot3([hCenter-rangeWidth,hCenter-rangeWidth],[vCenter-rangeMax,vCenter+rangeMax],[100,100],'-r');
    plot3([hCenter+rangeWidth,hCenter+rangeWidth],[vCenter-rangeMax,vCenter+rangeMax],[100,100],'-r');
else
    plot3([hCenter-rangeMin,hCenter-rangeMin],[vCenter-rangeWidth,vCenter+rangeWidth],[100,100],'-r');
    plot3([hCenter-rangeMax,hCenter-rangeMax],[vCenter-rangeWidth,vCenter+rangeWidth],[100,100],'-r');
    plot3([hCenter+rangeMin,hCenter+rangeMin],[vCenter-rangeWidth,vCenter+rangeWidth],[100,100],'-r');
    plot3([hCenter+rangeMax,hCenter+rangeMax],[vCenter-rangeWidth,vCenter+rangeWidth],[100,100],'-r');
    plot3([hCenter-rangeMax,hCenter+rangeMax],[vCenter-rangeWidth,vCenter-rangeWidth],[100,100],'-r');
    plot3([hCenter-rangeMax,hCenter+rangeMax],[vCenter+rangeWidth,vCenter+rangeWidth],[100,100],'-r');
end


subplot(222);
plot(histogram(:,1),log10(histogram(:,2)));
if(doAsymmetry)
subplot(223);
asymmetry(101) = 0;
plot(linspace(-1,1,201),(asymmetry))
end

