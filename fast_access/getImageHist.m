function [currentImage, delayHistogram] = getImageHistogram(currentDelayIndex)


%read in the image
currentImage = dlmread(['.\current_preview\', num2str(currentDelayIndex,'%03d'), '_image.dat'], '\t');        
%cut the border noise
if(1)
    width = 10;
    currentImage(1:width,:) = 0;
    currentImage(:,1:width) = 0;
    currentImage(1:width,end-width+1:end) = 0;
    currentImage(1:width,end-width+1:end) = 0;
end
%read in the histogram
delayHistogram = dlmread(fullfile('.\current_preview\',[num2str(currentDelayIndex,'%03d'),'_histogram.dat']),'\t');








