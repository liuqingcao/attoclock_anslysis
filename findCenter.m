function findCenter
clear all;
close all;
clc;

dataFolder = 'D:\Project_20171203\Recording_{1BD4E52A-55CA-47B4-B58F-93BCBF5CAFF9}';
load(fullfile(dataFolder,'scan.mat'));

xCenter = 239;
yCenter = 418;
intAngleHalf = 25;
screenRadius = 142;
rotate = 90;
peakList = [15,39,52,63];
cutList = [25,76];


xAxis = [1:600]-xCenter;
yAxis = [1:800]-yCenter;

if(0)
    currentImage = zeros(800,600);
    currentImage(yCenter-20:yCenter+20,:) = 1000;
else
    currentImage = scanSettings.summed_image;
end

%save image
clipImage = currentImage(yCenter+[-screenRadius:screenRadius],xCenter+[-screenRadius:screenRadius]);
size(clipImage)
save('clipImage.mat','clipImage','screenRadius','dataFolder');

%convert to cart
% ang_image = streaking_cycle_convert(log10(currentImage),xCenter,yCenter);
ang_image = streaking_cycle_convert(log10(clipImage),screenRadius+1,screenRadius+1);
%integral along radius
streakLeft = streaking_cycle_angle(ang_image, rotate, intAngleHalf,0);
streakRight = streaking_cycle_angle(ang_image, rotate, intAngleHalf,1);


%plot summed image
figure('position',get(0,'screenSize'),'color',[1 1 1]);
subplot(5,5,[1:3,6:8,11:13,16:18,21:23]);
imagesc(xAxis,yAxis,log10(currentImage));
axis equal;
hold on;
scatter(0,0,10);
for i=1:length(peakList)
    rectangle('Position',[-peakList(i),- peakList(i),2*peakList(i),2*peakList(i)], ...
        'Curvature',[1,1],'edgecolor','w','linestyle','-');
end
for i=1:length(cutList)
    rectangle('Position',[-cutList(i),- cutList(i),2*cutList(i),2*cutList(i)], ...
        'Curvature',[1,1],'edgecolor','r','linestyle','-');
end
rectangle('Position',[- screenRadius,- screenRadius,2*screenRadius,2*screenRadius], ...
    'Curvature',[1,1],'edgecolor','k','linewidth',2);
plot3([0,0],get(gca,'ylim'),[100,100],'k');
plot3(get(gca,'xlim'),[0,0],[100,100],'k');
plot3([-screenRadius*cosd(intAngleHalf),screenRadius*cosd(intAngleHalf)], ...
      [-screenRadius*sind(intAngleHalf),screenRadius*sind(intAngleHalf)], [100,100],'k');
plot3([-screenRadius*cosd(180-intAngleHalf),screenRadius*cosd(180-intAngleHalf)], ...
      [-screenRadius*sind(180-intAngleHalf),screenRadius*sind(180-intAngleHalf)], [100,100],'k');
plot3([0,0],get(gca,'ylim'),[100,100],'k');
set(gca,'ydir','norm');
xlim([-screenRadius,screenRadius]);
ylim([-screenRadius,screenRadius]);
xlabel('X (pixel)');
ylabel('Y (pixel)');



subplot(5,5,[4:5,9:10]);
imagesc(ang_image);
set(gca,'ydir','norm');
hold on;
for i=1:length(peakList)
    plot3([0,360],[peakList(i),peakList(i)],[100,100],'w');
end
for i=1:length(cutList)
    plot3([0,360],[cutList(i),cutList(i)],[100,100],'r');
end
xlim([0,359]);
ylim([0,screenRadius]);
xlabel('Angle (deg)');
ylabel('Radius (pixel)');

%

subplot(5,5,[14:15,19:20]);
plot(streakLeft,'r');
hold on;
plot(streakRight,'b');
for i=1:length(peakList)
    plot([peakList(i),peakList(i)],get(gca,'ylim'),'--k');
end
for i=1:length(cutList)
    plot([cutList(i),cutList(i)],get(gca,'ylim'),'-k');
end
xlim([0,screenRadius]);
ylim([0,inf]);
xlabel('Radius (pixel)');
ylabel('');


saveas(gcf,fullfile(dataFolder,'findCenter.fig'));



function ang_image = streaking_cycle_convert(dataImage,xCenter,yCenter)
    sx=size(dataImage,2);
    sy=size(dataImage,1);
    X=(1:sx) - xCenter;
    Y=(1:sy) - yCenter;
    thetaDeg = [0:1:359];
    theta=deg2rad(thetaDeg);
    R=0:xCenter-1;
    for i=1:length(theta)
        [xprime(:,i),yprime(:,i)]=pol2cart(theta(i),R);
    end
	ang_image = interp2(X,Y,dataImage,xprime,yprime,'*linear');
    ang_image(isnan(ang_image)) = 0;
    ang_image(isinf(ang_image)) = 0;
    
function streaking = streaking_cycle_angle(AngleImage, rotate, StreakAngle,updown)
    if(updown == 1) %part-1
        ang1 = 90 - rotate - StreakAngle;
        ang2 = 90 - rotate + StreakAngle;
    else %the opposite part
        ang1 = 270 - rotate - StreakAngle;
        ang2 = 270 - rotate + StreakAngle;      
    end
    if(ang1 <= 0)
        ang1 = ang1 + 360;
    end
    if(ang2 <= 0)
        ang2 = ang2 + 360;
    end
    if(ang1 < ang2) % normal
        streaking = sum(AngleImage(:,ang1:ang2),2);
    else % cross the 360 degree
        streaking = sum(AngleImage(:,ang1:360),2);
        streaking = streaking + sum(AngleImage(:,1:ang2),2);
    end
    
function rad = deg2rad(deg)
    rad = (pi/180).*deg;


