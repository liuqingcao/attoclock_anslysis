function rerange_raw_data(EventsLower,EventsUpper)
global scanSettings;
global tomo;
tic;

toolPath = '.\tool';
toolExe = 'vmi_analysis_streak_full.exe';
TempFolder = '.\current_preview';

%initial the array
delayImage = zeros(length(scanSettings.delays), scanSettings.hResolution, scanSettings.vResolution);        
for currentDelayIndex=1:%length(scanSettings.delays)
    disp([num2str(scanSettings.delays(currentDelayIndex),'%5.2f'),'/',num2str(currentDelayIndex),'/',num2str(length(scanSettings.delays))]);
    %use the C++ program to deal with the raw data
    command = [fullfile(toolPath,toolExe), ...
               ' -s',num2str(scanSettings.startShotIndices(currentDelayIndex)), ...
               ' -e', num2str(scanSettings.endShotIndices(currentDelayIndex)), ...
               ' -d',num2str(currentDelayIndex), ...
               ' -t',num2str(scanSettings.thresholdReductionSize), ...
               ' -f', scanSettings.DataPath, ...
               ' -o', scanSettings.DataFolder, ...
               ' -g', num2str(0), ...
               ' -h', num2str(scanSettings.hResolution), ...
               ' -v',  num2str(scanSettings.vResolution), ...
               ' -a',  num2str(EventsLower), ...
               ' -b',  num2str(EventsUpper) ];
    if(system(command) == 1)%if anything goes wrong
        h = msgbox(['The vmi_analysis_streak_post ',command,' is wrong! Please check the Current Working Path in Matlab'],'run_post_rerange.m','error');
        uiwait(h);
        return;
    end
    %readin the dat file
    fileName = fullfile(TempFolder, [num2str(currentDelayIndex,'%03d'), '_image.dat']);
    if(exist(fileName,'file'))
        delayImage(currentDelayIndex, :, :) = dlmread(fileName, '\t');   
    else
        h = msgbox(['file ',fileName,' is not exist!'],'run_post_rerange','error');
        uiwait(h);
        return;
    end
end
%save images
save(fullfile(tomo.dataFolder,['images_',num2str(EventsLower),'_',num2str(EventsUpper),'.mat']),'delayImage');
toc;
fr=figure('Visible','off','Color',[1 1 1]);
set(gca,'FontSize',16);
frame = 0;
for i=1:length(scanSettings.delays)
    imagesc([1:scanSettings.vResolution],[1:scanSettings.hResolution],log10(squeeze(delayImage(i,:,:))));
    c=colorbar('FontSize',14);
    set(gca,'YDir','normal');
%     caxis([0,2]);
    axis equal;
    box on;
    grid off;
    drawnow;
    frame = frame + 1;
    M(frame) = getframe(fr);
end
close(fr);
fileName = fullfile(tomo.dataFolder,['images_',num2str(EventsLower),'_',num2str(EventsUpper),'.gif']);
timeGap = 0.2;
for i = 1:frame
    [image,map] = frame2im(M(i));
    [im,map2] = rgb2ind(image,128);
    if i==1
        imwrite(im,map2,fileName,'GIF','WriteMode','overwrite','DelayTime',1,'LoopCount',inf);
    else
        imwrite(im,map2,fileName,'WriteMode','append','DelayTime',timeGap); %,'LoopCount',inf);
    end
end  
toc;