%tomography for all slices
function tomography_all
global tomo;
assignin('base','tomo',tomo);

width = 15;
tomo.sliceMin = tomo.vCenter - width;
tomo.sliceMax = tomo.vCenter + width;
tomo.sliceStepsize = 1;
tomo.sliceNumber = floor((tomo.sliceMax-tomo.sliceMin)/tomo.sliceStepsize);
tomo.tomoAllx = zeros(1,tomo.sliceNumber);
tomo.tomoAll = zeros(tomo.sliceNumber,2*tomo.streakRadius+1,2*tomo.streakRadius+1);
[~,index1] = min(abs(tomo.times - tomo.startTime));
[~,index2] = min(abs(tomo.times - (tomo.startTime+tomo.timeDuration)));
for i=1:tomo.sliceNumber
    disp(['tomography ',num2str(i),'/',num2str(tomo.sliceNumber)]);
    index = tomo.sliceMin + (i-1)*tomo.sliceStepsize;
    tomo.tomoAllx(i) = mean(index:index+tomo.sliceStepsize-1) - tomo.vCenter;
    sliceMean = squeeze(mean(tomo.imagesNorm(index1:index2,index:index+tomo.sliceStepsize-1,tomo.hCenter-tomo.streakRadius:tomo.hCenter+tomo.streakRadius),2));
    sliceMean = sliceMean - repmat(mean(sliceMean,1),[index2-index1+1,1]);
    sliceMean = sliceMean * 1e6;
    tomoMean = back(sliceMean', tomo.angleAxis)* 1e6;
    tomoMean(tomoMean<0) = 0;
    tomo.tomoAll(i,:,:) = tomoMean;
end

% sliceomatic(tomo.tomoAll);


figure('number','off','name',['all_',num2str(tomo.EventsLower),'_',num2str(tomo.EventsUpper)]);
angle1 = 26;
x1 = round(tomo.streakRadius/cosd(angle1));
hslice1 = surf(zeros(x1,1), tomo.tomoAllx, repmat([0:x1-1],[tomo.sliceNumber,1]));
rotate(hslice1,[0,1,0],angle1);
xd1 = get(hslice1,'XData');
yd1 = get(hslice1,'YData');
zd1 = get(hslice1,'ZData');
xd1 = xd1-min(min(xd1));
delete(hslice1);
angle2 = 30-angle1;
x2 = round(tomo.streakRadius/cosd(angle2));
if(isinf(x2))
   x2 = tomo.streakRadius + 1;
end
hslice2 = surf([1-x2:0], tomo.tomoAllx, zeros(tomo.sliceNumber,x2));
rotate(hslice2,[0,1,0],-angle2);
xd2 = get(hslice2,'XData');
yd2 = get(hslice2,'YData');
zd2 = get(hslice2,'ZData');
xd2 = xd2+abs(max(max(xd2)));
zd2 = zd2-abs(max(max(zd2)));
delete(hslice2);
angle3 = 30+angle1;
x3 = round(tomo.streakRadius/cosd(angle3));
if(isinf(x2))
   x3 = tomo.streakRadius + 1;
end
hslice3 = surf([0:x3-1], tomo.tomoAllx, zeros(tomo.sliceNumber,x3));
rotate(hslice3,[0,1,0],angle3);
xd3 = get(hslice3,'XData');
yd3 = get(hslice3,'YData');
zd3 = get(hslice3,'ZData');
xd3 = xd3-abs(min(min(xd3)));
zd3 = zd3-abs(max(max(zd3)));
delete(hslice3);

[x,y,z] = meshgrid( [-tomo.streakRadius:tomo.streakRadius],tomo.tomoAllx,[-tomo.streakRadius:tomo.streakRadius]);
% h=slice(x1,y,z,tomo.tomoAll, [xd1,xd2], [yd1,yd2], [zd1,zd2]);    % display the slices
h1=slice(x,y,z,tomo.tomoAll, [xd1], [yd1], [zd1]);    % display the slices
set(h1,'FaceColor','interp','EdgeColor','none', 'DiffuseStrength',0.8);
hold on;
h2=slice(x,y,z,tomo.tomoAll, [xd2], [yd2], [zd2]);    % display the slices
set(h2,'FaceColor','interp','EdgeColor','none', 'DiffuseStrength',0.8);
h3=slice(x,y,z,tomo.tomoAll, [xd3], [yd3], [zd3]);    % display the slices
set(h3,'FaceColor','interp','EdgeColor','none', 'DiffuseStrength',0.8);
%lines
radius = 40;
mArrow3([0 tomo.tomoAllx(end)+1 0],[radius*sind(angle1) tomo.tomoAllx(end)+1 radius*cosd(angle1)], 'facealpha', 1, 'color', 'red', 'stemWidth', 0.3); 
mArrow3([0 tomo.tomoAllx(end)+1 0],[-radius*cosd(angle2) tomo.tomoAllx(end)+1 -radius*sind(angle2)], 'facealpha', 1, 'color', 'red', 'stemWidth', 0.3); 
mArrow3([0 tomo.tomoAllx(end)+1 0],[radius*cosd(angle3) tomo.tomoAllx(end)+1 -radius*sind(angle3)], 'facealpha', 1, 'color', 'red', 'stemWidth', 0.3); 

%use special colormap with white background
ColorNum = 256;
WhiteRatio = 0.1;
WhiteNum = max(1,round(ColorNum * WhiteRatio));
cmap = colormap(jet(ColorNum)); 
cmap(1:WhiteNum,1) = (1-(0:WhiteNum-1)/WhiteNum);
cmap(1:WhiteNum,2) = cmap(1:WhiteNum,1);
cmap(1:WhiteNum,3) = 1;
set(gcf, 'Colormap', cmap);
xlim([-tomo.streakRadius,tomo.streakRadius]);
ylim([tomo.tomoAllx(1),tomo.tomoAllx(end)]);
zlim([-tomo.streakRadius,tomo.streakRadius]);
set(gca,'YDir','reverse')
view(-51,24)
ylabel('propagation');
grid off;
% box on;

assignin('base','tomo',tomo);



