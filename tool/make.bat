
set PATH=C:\MinGW\bin;%PATH%
set CC=g++
set CCFLAGS=-O3 -static-libgcc -static-libstdc++
set LIBS=-lm

%CC% %CCFLAGS% %LIBS% -Wno-write-strings  -o vmi_analysis_streak_full.exe vmi_analysis_streak_full.cpp

pause