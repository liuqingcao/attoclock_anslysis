//super software

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <getopt.h>
#include <windows.h>
#include <limits.h>
#include <errno.h>
#include <io.h>
#include <sstream>


#define _FILE_OFFSET_BITS == 64

using namespace std;
/// global variables


// settings
short additionalThreshold = 1;
int hResolution = 800;
int vResolution = 600;
int thresholdReductionSize = 1024;

int hCenter = 400;
int vCenter = 300;

int rangeRotate = 0;
int rangeMin = 100;
int rangeMax = 150;
int rangeWidth = 50;
double asymmetry = 0;
int doAsymmetry = 0;

long maxNumberFrames;

long startFrame=0;
long endFrame=0;
unsigned int nEventsLower = 0;
unsigned int nEventsUpper = 0;

int numberDelaystep = 0;

int generatePathFiles = 1;

//def
struct Event {
  short x;
  short y;
  short i;
};

//to be read from input
char outputPath[400];

//other stuff
unsigned long numberEvents;
long int numberOfFrames;
long int numberOfPhases;

//important
FILE* summedImageFiles;
FILE* rawImageFile;
FILE* histogramFile;
FILE* asymmetryFile;
char* rawImageFileName;


//important for the image analysis
unsigned int** pixelIsDone;
unsigned long** summedImage;
unsigned long* histogram;
double* asymmetryHist;
unsigned long droppedLower;
unsigned long droppedUpper;

char baseDirectory[400] = "d:\\";
char appDirectory[400];
char foundProjectDirectory[400];
char foundScanDirectory[400];
char dataDirectory[400];

int findFileMyself() {

    struct tm* clock;				// create a time structure
    time_t lastChangedTime = 0;
    struct stat attrib;			// create a file attribute structure

    //_tprintf (TEXT("Target file is %s\n"), argv[1]);
    WIN32_FIND_DATA FindFileData;
    HANDLE hFind;
    SetCurrentDirectory(baseDirectory);
    hFind = FindFirstFile("*", &FindFileData); //skip \.
    FindNextFile(hFind, &FindFileData); // and \..

    while (FindNextFile(hFind, &FindFileData) != 0) {
       if (hFind == INVALID_HANDLE_VALUE)
       {
          printf ("FindFirstFile failed (%d)\n", GetLastError());
          return 0;
       } else {
          stat(FindFileData.cFileName, &attrib); // get the attributes of entry
          if (S_ISDIR(attrib.st_mode)) { //only use if it is a directory
          printf(("%s modified: %s"),FindFileData.cFileName, ctime(&(attrib.st_mtime)));

          //printf ("Modified :  %2Ld\n", attrib.st_mtime);
          if (lastChangedTime < attrib.st_mtime) {
            lastChangedTime =  attrib.st_mtime;
            sprintf(foundProjectDirectory, "%s", FindFileData.cFileName );
          }
        }
       }
    }
    FindClose(hFind);
    printf("-----------------\nUsing %s as project directory\n\n--------------------\n", foundProjectDirectory);
    SetCurrentDirectory(foundProjectDirectory);
	GetCurrentDirectory(sizeof(foundProjectDirectory),foundProjectDirectory);
	
    lastChangedTime = 0;
    hFind = FindFirstFile("*", &FindFileData); //skip \.
    FindNextFile(hFind, &FindFileData); // and \..

    while (FindNextFile(hFind, &FindFileData) != 0) {
       if (hFind == INVALID_HANDLE_VALUE)
       {
          printf ("FindFirstFile failed (%d)\n", GetLastError());
          return 0;
       } else {
          stat(FindFileData.cFileName, &attrib); // get the attributes of entry
          if (S_ISDIR(attrib.st_mode)) { //only use if it is a directory
          printf(("%s modified: %s"),FindFileData.cFileName, ctime(&(attrib.st_mtime)));

          //printf ("Modified :  %2Ld\n", attrib.st_mtime);
          if (lastChangedTime < attrib.st_mtime) {
            lastChangedTime =  attrib.st_mtime;
            sprintf(foundScanDirectory, "%s", FindFileData.cFileName );
          }
        }
       }
    }
    FindClose(hFind);
	
	
	
    printf("-----------------\n\nUsing %s as scan directory\n--------------------\n", foundScanDirectory);
    printf("Looking for *.0 scan file...\n", foundScanDirectory);

    SetCurrentDirectory(foundScanDirectory);
	GetCurrentDirectory(sizeof(foundScanDirectory),foundScanDirectory);
    hFind = FindFirstFile("*.0", &FindFileData);
    if (hFind != INVALID_HANDLE_VALUE)
    {
        printf("Scan found: %s\n", FindFileData.cFileName);
        rawImageFileName = new char[400];
        sprintf(rawImageFileName, "%s", FindFileData.cFileName );
		sprintf(dataDirectory, "%s", FindFileData.cFileName );
    } else return 0;
    FindClose(hFind);
	
	//creat and clear folder
	CreateDirectory(outputPath, NULL);
	
	
	
	//print out the project path and data path
	char fName[400];
	sprintf(fName,"%s\\FilePath.txt", outputPath);
	FILE* tmpFile = fopen(fName,"w+");
	fprintf(tmpFile, "%s\\\n", foundScanDirectory);
	fprintf(tmpFile, "%s\\%s\n", foundScanDirectory, rawImageFileName);
	fclose(tmpFile);
	

    return 1;
	
}

void initAll(){
    cout << "Initialising... ";
	char fName[400];
	//check the rawImageFile
	
	cout << _access(dataDirectory, 06) <<endl; 
	printf( "Error  _access is : %s\n", strerror( errno ) );
    rawImageFile = fopen64(dataDirectory,"rb");
	printf( "Error  opening is : %s\n", strerror( errno ) );

    cout << "Opening raw camera file..." << flush;

    //init arrays
    summedImage = new unsigned long*[hResolution];
	for (int nH = 0; nH < hResolution; nH++){
        summedImage[nH] = new unsigned long[vResolution];
		for (int nV = 0; nV < vResolution; nV++){
            summedImage[nH][nV] = 0;
		}
	}

    sprintf(fName,"%s\\%03d_histogram.dat",outputPath, numberDelaystep);
    histogramFile = fopen(fName,"w");
    //init histogram
    histogram = new unsigned long[thresholdReductionSize+1];
    for (int n =0; n < thresholdReductionSize+1; n++) {
        histogram[n] = 0;
    }
	
	if(doAsymmetry){
		sprintf(fName,"%s\\%03d_asymmetry.dat",outputPath, numberDelaystep);
		asymmetryFile = fopen(fName,"w");
		//init asymmetry
		asymmetryHist = new double[201];
		for (int n =0; n < 201; n++) {
			asymmetryHist[n] = 0;
    }
		
		
	}
	

    cout << " Done!" << endl;
}

void addFrame(Event* frame){

 	//cout <<"new frame" << endl;
    for (int nEvent = 0; nEvent < thresholdReductionSize; nEvent++) {
        //fprintf(binnedAngleFiles[nAngle],"%d\t%d\t%d\n",frame[nEvent].x, frame[nEvent].y, frame[nEvent].i);
        if (frame[nEvent].i >  additionalThreshold) {
			//cout << "x: " << frame[nEvent].x << " y: " <<frame[nEvent].y << " i: " << frame[nEvent].i << endl;
			if (( (frame[nEvent].x) > -1) && ( (frame[nEvent].y) > -1) &&( (frame[nEvent].x) < hResolution) && ( (frame[nEvent].y) < vResolution)){
				summedImage[frame[nEvent].x][frame[nEvent].y ] +=1;
				numberEvents+=1;
			}
		}
    }
	//cout << numberEvents << endl;
}


void createImages(){

    char fName[400];
	//SetCurrentDirectory(outputPath);
    sprintf(fName,"%s\\%03d_image.dat",outputPath, numberDelaystep);
    FILE* asciiFile = fopen(fName,"w+");
	if (asciiFile!=NULL){
	
    for (int nx = 0; nx < hResolution; nx++) {
    for (int ny = 0; ny < vResolution-1; ny++) {
       // printf("%d\t", currentFrameASCII[nx][ny]);
        fprintf(asciiFile,"%d\t", summedImage[nx][ny]);
    }
        fprintf(asciiFile,"%d\n", summedImage[nx][vResolution-1]);
    }
    fclose(asciiFile);
	} else cout << "error opening output file" << endl;
}


int readRawImageFrame(Event* newFrame, int64_t nFrame){
	
 int nEvents = 0;
 int left = 0;
 int right = 0;
 if (fseeko64(rawImageFile, nFrame*thresholdReductionSize*6, SEEK_SET) == -1);//cout << errno << endl;
    //printf( "seek means: %s\n", strerror( errno ) );
	 Event p;
  //allocate some buffer
  short *t=new short[3*thresholdReductionSize];
  size_t tmp = fread(t, sizeof(short),3*thresholdReductionSize,rawImageFile);
  
  if (tmp < 3*thresholdReductionSize) {
  //cout << "Error at " << nFrame << " andf " << thresholdReductionSize << endl;
  
  } else {
	  for (int np = 0; np < thresholdReductionSize; np++){
		  p.x = *(t+np*3);
		  p.y = *(t+np*3+1);
		  p.i = *(t+np*3+2);
		  newFrame[np] = p;
		  //cout << p.x <<""<<p.y<<"" <<p.i <<"";
		  if (p.i >  additionalThreshold){
			nEvents++; //count events in each frame
			if(doAsymmetry){ //calculate the count in two areas
				if(rangeRotate){ //up and down
					if(p.x >= hCenter - rangeWidth && p.x <= hCenter + rangeWidth){
						if(p.y >= rangeMin + vCenter && p.y <= rangeMax + vCenter) left++;
						if(p.y <= vCenter - rangeMin && p.y >= vCenter - rangeMax) right++;
					}
				} else { //left and right
					if(p.y >= vCenter - rangeWidth && p.y <= vCenter + rangeWidth){
						if(p.x >= rangeMin + hCenter && p.x <= rangeMax + hCenter) left++;
						if(p.x <= hCenter - rangeMin && p.x >= hCenter - rangeMax) right++;
						//cout<<"here, left: "<<left<<", right: "<<right<<", x: "<<p.x<<", y: "<<p.y<<endl;
					}
				}
			}
		  }
	  }
  }
  if(doAsymmetry){
  	asymmetry = (left-right)/(left+right+1e-5);
	//cout<<"asymmetry: "<<asymmetry<<", left: "<<left<<", right: "<<right<<endl;
  }
  delete[] t;
  return nEvents;
}


int main(int argc, char** argv){

	thresholdReductionSize = 1024;
	nEventsUpper = thresholdReductionSize;
	droppedLower = 0;
	droppedUpper = 0;

	//get the path of the application
	GetCurrentDirectory(sizeof(appDirectory),appDirectory);
	sprintf(outputPath, "%s\\current_preview\\",appDirectory);
	std::string word;
	std::string word1;
	int c;
    std::stringstream stream;
	std::stringstream stream1;
	long startFrameAll[100];
	long endFrameAll[100];
	long index, index1;
	while ((c = getopt(argc, argv, "n:t:o:c:h:v:g:s:e:a:b:p:d:f:x:y:m:l:u:r:w:")) != -1)
          switch (c) {					
              case 'n':
                   //numberOfAngles = atoi(optarg);
                   break;
              case 't':
                   thresholdReductionSize = atoi(optarg);
				   nEventsUpper = thresholdReductionSize;
                   break;
              case 'o':
				   strcpy(foundScanDirectory, optarg);
                   break;
              case 'c':
                   //cepStatisticsSize = atoi(optarg);
                   break;
              case 'h':
                   hResolution = atoi(optarg);
                   break;
              case 'v':
                   vResolution = atoi(optarg);
                   break;
              case 'g':
                   generatePathFiles = atoi(optarg);
                   break;
              case 's':
				index = 0;
				stream << optarg;
				while( getline(stream, word, ',') ){
					startFrameAll[index] = atoi(word.c_str());
					cout<<index<<" "<<word<<" "<<startFrameAll[index]<<endl;
					index++;
				}
                   break;
              case 'e':
				index1 = 0;
				stream1 << optarg;
				while( getline(stream1, word1, ',') ){
					endFrameAll[index1] = atoi(word1.c_str());
					cout<<index1<<" "<<word1<<" "<<endFrameAll[index1]<<endl;
					index1++;
				}
                   break;
			  case 'a':
                   nEventsLower = atoi(optarg);
                   break;
			  case 'b':
                   nEventsUpper = atoi(optarg);
                   break;
			  case 'p':
                   //phaseScaler = atoi(optarg);
				   strcpy(baseDirectory, optarg);
                   break;
			  case 'd':
					numberDelaystep = atoi(optarg);
					break;
			  case 'f':
					strcpy(dataDirectory, optarg);
					break;	
			  case 'x':
					hCenter = atoi(optarg);
					break;
			  case 'y':
					vCenter = atoi(optarg);
					break;	
			  case 'm':
					doAsymmetry = atoi(optarg);
					break;
			  case 'l':
					rangeMin = atoi(optarg);
					break;
			  case 'u':
					rangeMax = atoi(optarg);
					break;		
			  case 'r':
					rangeRotate = atoi(optarg);
					break;
			  case 'w':
					rangeWidth = atoi(optarg);
					break;					
			}
	if(generatePathFiles > 0){
		if (findFileMyself()) {
			cout << "File found... continuing!" << endl;
		} else {
			cout << "Nothing found... exiting!" << endl;
			return 0;
		}
	}
	
	cout<<"outputPath  "<<outputPath<<endl;
	cout << "Settings" << endl;
	cout<< "Raw file path: " << foundScanDirectory << endl;
	cout << "Raw input file: " << dataDirectory << endl;
	cout << "Threshold Reduction Size: " << thresholdReductionSize << endl;
	cout << "hResolution: " << hResolution << endl;
	cout << "vResolution: " << vResolution << endl;
	cout << "count resolution of interest from: " << nEventsLower << endl;
	cout << "to: " << nEventsUpper << endl;
	
	initAll();

	//time for creating arrays
	Event* frame = new Event[thresholdReductionSize];
	long nEvents;

	
	if (1) {
		cout << index<<endl;
	for (long int currentRange = 0;	currentRange < index; currentRange++){
		startFrame = startFrameAll[currentRange];
		endFrame = endFrameAll[currentRange];
		cout << currentRange<<": Running from " << startFrame << " to " << endFrame <<  endl;
	for (long int currentFrame = startFrame; currentFrame < endFrame; currentFrame++) {

		//read data

		nEvents = readRawImageFrame(frame, currentFrame);
		 //cout << "read image" << endl;

		if ((currentFrame%1000) == 0) printf("Frame %d/%d \r", currentFrame, endFrame - startFrame );
		//decide which file it goes to
	   
	   if ((nEvents > nEventsLower) && (nEvents < nEventsUpper+1)) {
	   
			addFrame(frame);
	  // cout << " added frame " << endl;

	   } //else cout << "nevents error " << nEvents << endl;
	   
	   if (nEvents < nEventsLower) droppedLower++;
	   if (nEvents > nEventsUpper+1) droppedUpper++;
		  
		histogram[nEvents] = histogram[nEvents]+1;
		if ((nEvents < 0) | (nEvents>thresholdReductionSize)) cout << "nevents error " << nEvents << endl;
		
		
		
		if(doAsymmetry){
			int index = (asymmetry+1)*100;
			asymmetryHist[index] += 1;
		}
		
	}
	}


	for (int n =0; n < thresholdReductionSize+1; n++) {
		fprintf(histogramFile,"%d\t%d\n", n, histogram[n]);
	}
	
	if(doAsymmetry){
		for (int n =0; n < 201; n++) {
			fprintf(asymmetryFile,"%f\n", asymmetryHist[n]);
		}
	}
	

	fclose(histogramFile);
	fclose(rawImageFile);
	if(doAsymmetry) fclose(asymmetryFile);

	cout << "Dropped " << droppedLower << " frames (too few events)" << endl;
	cout << "Dropped " << droppedUpper << " frames (too many events)" << endl;
	//now create image files
	cout << "Creating images..." << endl;
	createImages();
	cout << "Done!" << endl;

	}

}

